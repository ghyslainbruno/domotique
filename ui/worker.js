importScripts('socket.io-client/dist/socket.io.js');

var url = {};

onmessage = function(event) {
    url = event.data;
};


var socket = io.connect(url, {
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionAttempts: Infinity
});

socket.on('databseChange', function(testFromServer) {
    postMessage("databaseChanges");
});

socket.on('connect', function(testFromServer) {
    postMessage("connect");
});

setInterval(function() {
    if (!socket.connected) {
        postMessage("disconnected");
        // socket = io.connect(url);
    }

    if (socket.connected) {
        postMessage("connect");
    }
}, 1000);