const express = require('express');
const app = express();
var sys = require('sys');
var bodyParser = require('body-parser');
// var auth = require('basic-auth');
var exec = require('child_process').exec;
var fileCredentials = require('./credentials.json');
var jsonDB = require('node-json-db');
// var chokidar = require('chokidar');
// var delay = require('delay');
var events = require('events');


var eventEmitter = new events.EventEmitter();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// var rpi433    = require('rpi-433'),
//     rfSniffer = rpi433.sniffer({
//       pin: 2,                     //Snif on GPIO 2 (or Physical PIN 13)
//       debounceDelay: 700          //Wait 500ms before reading another code
//     }),
//     rfEmitter = rpi433.emitter({
//       pin: 0,                     //Send through GPIO 0 (or Physical PIN 11)
//       pulseLength: 700,            //Send the code with a 350 pulse length
//     });

var db = new jsonDB("states", true, false);

// Initialize JsonDB
// db.push('/shutters/position', 'down');
// db.push('/shutters/last', 'down');

// app.use(function(req, res, next){
//     var user = auth(req);
//
//     if (user === undefined || user['name'] !== fileCredentials.user || user['pass'] !== fileCredentials.password) {
//         res.statusCode = 401;
//         res.setHeader('WWW-Authenticate', 'Basic realm="NodeJS"');
//         res.end('Unauthorized');
//     } else {
//         next();
//     }
// });


// Receive (data is like {code: xxx, pulseLength: xxx}) 
// rfSniffer.on('data', function (data) {
//   console.log(Date() + ' - Code received: '+data.code+' pulse length : '+data.pulseLength);
// });


app.post('/on', function(req, res) {
	var id = req.query.id;

    switchOn(id, function() {
        //db.save();

        // Trigger event to say to all clients to update their states
        eventEmitter.emit('pushGetToClients');
        res.send('Light ' + id + ' On');
    });
});

app.post('/authorized-users', function(req, res){
   if (req.body.uid === 'C9BLuhtsggTeyBzvD9WZ5Ps4WjD3' || req.body.uid ==='yksJ93CLGeUhQrJBWNQYxo84pU32') {
       res.send(true)
   } else {
       res.statusCode = 401;
       // res.setHeader('WWW-Authenticate', 'Basic realm="NodeJS"');
       res.end('Unauthorized');
       res.send(false);
   }
});

app.post('/off', function(req, res) {
    var id = req.query.id;

    switchOff(id, function() {
        //db.save();
        eventEmitter.emit('pushGetToClients');
        res.send('Light ' + id + ' Off');
    });

});

app.get("/config", function(req, res) {
	res.send(db.getData("/"));
});

app.post('/all_on', function(req, res) {
    switchOnAll(function() {
        //db.save();
        res.send('All On');
    });
});

app.post('/all_off', function(req, res) {
    switchOffAll(function() {
        //db.save();
        res.send('All Off');
    });
});


var timer = {};

app.post('/up', function(req, res) {
	var shutters = db.getData('/shutters');

	up(shutters, function() {
        //db.save();
        res.send('Up sent');
    })

});

app.post('/pause', function(req, res) {
    var shutters = db.getData('/shutters');

    pause(shutters, function() {
        // //db.save();
        res.send('Pause sent');
    })

});

app.post('/down', function(req, res) {
    var shutters = db.getData('/shutters');

    down(shutters, function() {
        // //db.save();
        res.send('Down sent');
    })

});


// Lights switch methods
function switchOn(id, callback) {
    switch (id) {
        case "1":
            exec("/home/pi/433Utils/RPi_utils/codesend -1176279551 2 700 32", function (error, stdout, stderr) {
                // sys.print('stdout: ' + stdout);
                // sys.print('stderr: ' + stderr);
                if (error !== null) {
                    console.log(Date() + ' exec error: ' + error);
                } else {
                    // Put here the write into DB in prod environment
                    // db.push("/lamp1", true);
                    // callback();
                }
                db.push("/lamp1", true);
                eventEmitter.emit('pushGetToClients');
                callback();
            });
            // db.push("/lamp1", true);
            break;

        case "2":
            exec("/home/pi/433Utils/RPi_utils/codesend -1713150463 2 700 32", function (error, stdout, stderr) {
                // sys.print('stdout: ' + stdout);
                // sys.print('stderr: ' + stderr);
                if (error !== null) {
                    console.log(Date() + ' exec error: ' + error);
                } else {
                    // db.push("/lamp2", true);
                    // callback();
                }
                db.push("/lamp2", true);
                eventEmitter.emit('pushGetToClients');
                callback();
            });
            // db.push("/lamp2", true);
            break;

        default:
            callback();
            break;
    }
}

function switchOff(id, callback) {
    switch (id) {
        case "1":
            exec("/home/pi/433Utils/RPi_utils/codesend -1243388415 2 700 32", function (error, stdout, stderr) {
                // sys.print('stdout: ' + stdout);
                // sys.print('stderr: ' + stderr);
                if (error !== null) {
                    console.log(Date() + ' exec error: ' + error);
                } else {
                    // Put here the write into DB in prod environment
                    // db.push("lamp1", true);
                }
            });
            db.push("/lamp1", false);
            eventEmitter.emit('pushGetToClients');
            callback();
            break;

        case "2":
            exec("/home/pi/433Utils/RPi_utils/codesend -1780259327 2 700 32", function (error, stdout, stderr) {
                // sys.print('stdout: ' + stdout);
                // sys.print('stderr: ' + stderr);
                if (error !== null) {
                    console.log(Date() + ' exec error: ' + error);
                } else {
                    // db.push("lamp2", true);
                }
            });
            db.push("/lamp2", false);
            eventEmitter.emit('pushGetToClients');
            callback();
            break;

        default:
            callback();
            break;
    }
}

function switchOnAll(callback) {
    exec("/home/pi/433Utils/RPi_utils/codesend -1813813759 2 700 32", function (error, stdout, stderr) {
        // sys.print('stdout: ' + stdout);
        // sys.print('stderr: ' + stderr);
        if (error !== null) {
            console.log(Date() + ' exec error: ' + error);
        } else {
            // Put here the write into DB in prod environment
            // db.push("lamp1", true);
        }
    });
    db.push("/lamp1", true);
    db.push("/lamp2", true);
    eventEmitter.emit('pushGetToClients');
    callback();
}

function switchOffAll(callback) {
    exec("/home/pi/433Utils/RPi_utils/codesend -1545378303 2 700 32", function (error, stdout, stderr) {
        // sys.print('stdout: ' + stdout);
        // sys.print('stderr: ' + stderr);
        if (error !== null) {
            console.log(Date() + ' exec error: ' + error);
        } else {
            // Put here the write into DB in prod environment
            // db.push("lamp1", true);
        }
    });
    db.push("/lamp1", false);
    db.push("/lamp2", false);
    eventEmitter.emit('pushGetToClients');
    callback();
}


// Still need to be tested
function up(shutters, callback) {
    if (shutters.last === 'up') {

        console.log("not possible");
        callback();

    } else if (shutters.last === 'pause') {

        if (shutters.position === 'up') {
            console.log("not possible");
            callback();
        } else if (shutters.position === 'pause') {

            sendShuttersUp(function() {
                db.push('/shutters/last', 'up');
                db.push('/shutters/position', 'somewhere');
                eventEmitter.emit('pushGetToClients');

                clearTimeout(timer);
                timer = setTimeout(function() {
                    db.push('/shutters/position', 'up');
                    eventEmitter.emit('pushGetToClients');
                }, 30000);
                callback();
            });

        } else if (shutters.position === 'down') {
            console.log("not possible");
            callback();
        } else {
            callback();
        }

    } else if (shutters.last === 'down') {

        if (shutters.position === 'up') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'pause') {

            console.log("not possible");
            callback();

            // TODO : see if it's ok to put everything in the same statement
        } else if (shutters.position === 'down') {

            sendShuttersDown(function() {
                db.push('/shutters/last', 'up');
                eventEmitter.emit('pushGetToClients');

                sendShuttersUp(function() {
                    db.push('/shutters/position', 'somewhere');
                    eventEmitter.emit('pushGetToClients');
                    clearTimeout(timer);
                    timer = setTimeout(function() {
                        db.push('/shutters/position', 'up');
                        eventEmitter.emit('pushGetToClients');
                    }, 30000);
                    callback();
                });

            });


        } else if (shutters.position === 'somewhere') {

            sendShuttersDown(function () {
                db.push('/shutters/last', 'up');
                eventEmitter.emit('pushGetToClients');
                sendShuttersUp(function() {
                    db.push('/shutters/position', 'somewhere');
                    eventEmitter.emit('pushGetToClients');
                    clearTimeout(timer);
                    timer = setTimeout(function() {
                        db.push('/shutters/position', 'up');
                        eventEmitter.emit('pushGetToClients');
                    }, 30000);
                    callback();
                });

            });

        } else {
            callback();
        }
    } else {
        callback();
    }
}

function pause(shutters, callback) {
    if (shutters.last === 'up') {

        if (shutters.position === 'up') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'pause') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'down') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'somewhere') {

            clearTimeout(timer);
            sendShuttersUp(function () {
                db.push('/shutters/last', 'pause');
                db.push('/shutters/position', 'pause');
                eventEmitter.emit('pushGetToClients');
                callback();
            });
        } else {
            callback();
        }

    } else if (shutters.last === 'pause') {

        console.log("not possible");
        callback();

    } else if (shutters.last === 'down') {

        if (shutters.position === 'up') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'pause') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'down') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'somewhere') {

            clearTimeout(timer);
            sendShuttersDown(function () {
                db.push('/shutters/last', 'pause');
                db.push('/shutters/position', 'pause');
                eventEmitter.emit('pushGetToClients');
                callback();
            });
        } else {
            callback();
        }

    } else {
        callback();
    }
}

function down(shutters, callback) {
    if (shutters.last === 'up') {

        if (shutters.position === 'up') {
            sendShuttersUp(function() {
                db.push('/shutters/last', 'down');
                eventEmitter.emit('pushGetToClients');
                sendShuttersDown(function() {
                    db.push('/shutters/position', 'somewhere');
                    eventEmitter.emit('pushGetToClients');
                    clearTimeout(timer);
                    timer = setTimeout(function() {
                        db.push('/shutters/position', 'down');
                        eventEmitter.emit('pushGetToClients');
                    }, 30000);
                    callback();
                });
            });



        } else if (shutters.position === 'pause') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'down') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'somewhere') {

            sendShuttersUp(function() {
                db.push('/shutters/last', 'down');
                eventEmitter.emit('pushGetToClients');
                sendShuttersDown(function() {
                    db.push('/shutters/position', 'somewhere');
                    eventEmitter.emit('pushGetToClients');
                    clearTimeout(timer);
                    timer = setTimeout(function() {
                        db.push('/shutters/position', 'down');
                        eventEmitter.emit('pushGetToClients');
                    }, 30000);
                    callback();
                });

            });

        } else {
            callback();
        }

    } else if (shutters.last === 'pause') {

        if (shutters.position === 'up') {

            console.log("not possible");
            callback();

        } else if (shutters.position === 'pause') {

            sendShuttersDown(function() {
                db.push('/shutters/last', 'down');
                db.push('/shutters/position', 'somewhere');
                eventEmitter.emit('pushGetToClients');

                clearTimeout(timer);
                timer = setTimeout(function() {
                    db.push('/shutters/position', 'down');
                    eventEmitter.emit('pushGetToClients');
                }, 30000);
                callback();
            });


        } else if (shutters.position === 'down') {

            console.log("not possible");
            callback();

        } else {
            callback();
        }

    } else if (shutters.last === 'down') {

        console.log("not possible");
        callback();

    } else {
        callback();
    }
}


// Equivalent to buttons press on RF remote
function sendShuttersDown(callback) {
	// console.log('sending down');

    exec("/home/pi/433Utils/RPi_utils/codesend 7857156 1 300 24", function (error, stdout, stderr) {
        // sys.print('stdout: ' + stdout);
        // sys.print('stderr: ' + stderr);
        if (error !== null) {
            console.log(Date() + ' exec error: ' + error);
        } else {
            // Put here the write into DB in prod environment
            // db.push("lamp1", true);
        }
        setTimeout(function() {
            callback();
        }, 500);
    });

}

function sendShuttersUp(callback) {
    // console.log('sending up');

    exec("/home/pi/433Utils/RPi_utils/codesend 7857154 1 300 24", function (error, stdout, stderr) {
        // sys.print('stdout: ' + stdout);
        // sys.print('stderr: ' + stderr);
        if (error !== null) {
            console.log(Date() + ' exec error: ' + error);
        } else {
            // Put here the write into DB in prod environment
            // db.push("lamp1", true);
        }
        setTimeout(function() {
            callback();
        }, 500);
    });

}


// Purpose -> turn On all lights and open shutters
app.get('/morning', function(req, res, next) {

    var states = db.getData('/');

    if (states.shutters.position === 'somewhere') {
        // pause
        // go up
        // Implementation
        if (states.shutters.last === 'down') {
            pause(states.shutters, function() {
                up(states.shutters, function () {
                    // res.send('Morning mode sent');
                    next();
                })
            });
        } else {
            next();
        }
    } else if (states.shutters.position === 'down') {
        // go up
        up(states.shutters, function() {
            // res.send('Morning mode sent');
            next();
        })
    } else if (states.shutters.position === 'pause') {
        // go up
        up(states.shutters, function() {
            // res.send('Morning mode sent');
            next();
        })
    } else {
        next();
    }
});
// Lights part
app.get('/morning', function(req, res) {
    var states = db.getData('/');

    if (states.lamp1 === false && states.lamp2 === false) {
        switchOnAll(function() {
            // //db.save();
            res.send('Morning mode sent');
        });
    } else {
        if (!states.lamp1) {
            switchOn("1", function() {
                if (!states.lamp2) {
                    switchOn("2", function() {
                        // //db.save();
                        res.send('Morning mode sent');
                    });
                } else {
                    // //db.save();
                    res.send('Morning mode sent');
                }
            });
        } else {
            if (!states.lamp2) {
                switchOn("2", function() {
                    if (!states.lamp1) {
                        switchOn("1", function() {
                            //db.save();
                            res.send('Morning mode sent');
                        });
                    } else {
                        //db.save();
                        res.send('Morning mode sent');
                    }
                });
            } else {
                //db.save();
                res.send('Morning mode sent');
            }
        }

    }
});


// Purpose -> turn On all lights and close shutters
app.get('/evening', function(req, res, next) {
    var states = db.getData('/');

    if (states.shutters.position === 'somewhere') {
        // pause
        // go down
        // Implementation
        if (states.shutters.last === 'up') {
            pause(states.shutters, function() {
                down(states.shutters, function () {
                    // res.send('Evening mode sent');
                    next();
                })
            })
        } else {
            next();
        }

    } else if (states.shutters.position === 'up') {
        // go up
        down(states.shutters, function() {
            // res.send('Evening mode sent');
            next();
        })
    } else if (states.shutters.position === 'pause') {
        // go up
        down(states.shutters, function() {
            // res.send('Evening mode sent');
            next();
        })
    } else {
        next();
    }
});

app.get('/evening', function(req, res) {

    // Same piece of code that the 2nd morning part --> To refacto
    var states = db.getData('/');

    if (states.lamp1 === false && states.lamp2 === false) {
        switchOnAll(function() {
            //db.save();
            res.send('Morning mode sent');
        });
    } else {
        if (!states.lamp1) {
            switchOn("1", function() {
                if (!states.lamp2) {
                    switchOn("2", function() {
                        //db.save();
                        res.send('Evening mode sent');
                    });
                } else {
                    //db.save();
                    res.send('Evening mode sent');
                }
            });
        } else {
            if (!states.lamp2) {
                switchOn("2", function() {
                    if (!states.lamp1) {
                        switchOn("1", function() {
                            //db.save();
                            res.send('Evening mode sent');
                        });
                    } else {
                        //db.save();
                        res.send('Evening mode sent');
                    }
                });
            } else {
                //db.save();
                res.send('Evening mode sent');
            }
        }

    }
});


app.get('/night', function (req, res, next) {
    var states = db.getData('/');

    if (states.shutters.position === 'somewhere') {
        // pause
        // go down
        // Implementation
        if (states.shutters.last === 'up') {
            pause(states.shutters, function() {
                down(states.shutters, function () {
                    // res.send('Night mode sent');
                    next();
                });
            })
        } else {
            // res.send('Night mode sent');
            next();
        }
    } else if (states.shutters.position === 'up') {
        // go up
        down(states.shutters, function() {
            // res.send('Night mode sent');
            next();
        })
    } else if (states.shutters.position === 'pause') {
        // go up
        down(states.shutters, function() {
            // res.send('Night mode sent');
            next();
        })
    } else {
        next();
    }
});

app.get('/night', function(req, res) {
    var states = db.getData('/');

    if (states.lamp1 === true && states.lamp2 === true) {
        switchOffAll(function() {
            //db.save();
            res.send('Night mode sent');
        });
    } else {
        if (states.lamp1) {
            switchOff("1", function() {
                if (states.lamp2) {
                    switchOff("2", function() {
                        //db.save();
                        res.send('Night mode sent');
                    });
                } else {
                    //db.save();
                    res.send('Night mode sent');
                }
            });
        } else {
            if (states.lamp2) {
                switchOff("2", function() {
                    //db.save();
                    res.send('Night mode sent');
                });
            } else {
                //db.save();
                res.send('Night mode sent');
            }
        }
    }
});



//////// Connection part //////////

app.get('/websowket_url', function(req, res) {
    res.send(process.env.WEB_SOCKET_URL + ':' + process.env.PORT);
});


app.use(express.static('ui'));
app.use(express.static('node_modules'));


var server = app.listen(process.env.PORT, function () {
    console.log('Server starting - listening on port ' + process.env.PORT)
});


// WebSockets use
var io = require('socket.io').listen(server);


// eventEmitter.on('pushGetToClients', function() {
//     io.sockets.emit('databseChange', "toto from server")
// });

var isfirstTime = true;

io.sockets.on('connection', function (socket) {

    // var watcher = chokidar.watch('states.json');
    //
    // watcher.on('change', path =>{
    //     io.sockets.emit('databseChange', "toto from server")
    // });

    if (isfirstTime) {
        eventEmitter.on('pushGetToClients', function() {
            io.sockets.emit('databseChange', "toto from server")
        });

        isfirstTime = false;
    }

});
